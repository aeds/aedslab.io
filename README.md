## Estudo de algoritmos e exemplos de códigos

Buscamos explicar e exemplificar algoritmos utilizados nas disciplidas de
**Algoritmos e Estruturas de Dados 1, 2 e 3 (AEDS)** ministrada na [Universidade
Federal de Minas Gerais (UFMG)](https://www.ufmg.br/) com objetivo de facilitar
o **estudo** e **codificação** dos algoritmos.

Para facilitar a busca pelo tema de interesse do aluno é possível filtrar os
posts por [#tags](tags).

## Plágio
Os códigos aqui são apenas **exemplos** e não devem ser copiados para trablhos
disciplinares. A cópia de algum deles será identificada pelo **MOSS** e pode
resultar em nota 0 além de **processo disciplinar**.

---
title: Tutores
subtitle: Contato de professores particulares
date: 2017-08-17
tags: ["tutores", "Aulas"]
comments: true
---

### Amanda Souza

| Disciplinas | AEDS 1 |
| :-: | :-: |
| Conhecimentos especificos: | ALLEGRO |
| Preço: | R$ 30,00 / 50 minutos |
| Telefone | +55 31 97515-5687 (tim) | 
| Email | vrchl.souza@gmail.com |

---

### Artur Marzano

> I love Pusheen.

| Disciplinas | AEDS 1, AEDS 2 e AEDS 3 |
| :-: | :-: |
| Preço: AEDS 1 | R$ 30,00 / 50 minutos |
| Preço: AEDS 2 | R$ 35,00 / 50 minutos |
| Preço: AEDS 3 | R$ 40,00 / 50 minutos |
| Telefone | +55 31 97574-7012 (tim) | 


---

### João Vitor Tenório 

| Disciplinas | AEDS 1, AEDS 2, AEDS 3, Mineração de Dados e Programação Modular |
| :-: | :-: |
| Preço: | R$ 50,00 / 50 minutos |
| Telefone | +55 87 9996-30538 |

---

### Julio Guimarães

| Disciplinas | AEDS 1, AEDS 2 e AEDS 3 |
| :-: | :-: |
| Preço: | R$ 50,00 / 50 minutos |
| Telefone | +55 31 99771-1883 (vivo) |
| Email | julioctg33@gmail.com |

---

### Pedro Ramos

| Disciplinas | AEDS 1, AEDS 2 e AEDS 3 |
| :-: | :-: |
| Preço: | R$ 40,00 / 50 minutos |
| Telefone | +55 31 99504-5858 (vivo) |
| Email | phramoscosta@gmail.com |

---

### Vinicius Brenner

| Disciplinas | AEDS 1 e AEDS 2 |
| :-: | :-: |
| Preço: AEDS 1 | R$ 30,00 / 50 minutos |
| Preço: AEDS 2 | R$ 35,00 / 50 minutos |
| Telefone | +55 31 99234-8412 (vivo) |
| Email | vinicius_brennerr@gmail.com |


---

### Aviso
A equipe de desenvolvimento desse projeto **não assume nenhuma responsabilidade**
pela relação entre aluno e tutor. A integridade e/ou qualidade das das aulas
ministradas pelos tutores são de responsabilidade dos mesmos e fica a critério
do aluno o contato e contrato com o tutor.

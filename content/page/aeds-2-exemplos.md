---
title: AEDS 2
subtitle: Alguns exemplos de trabalhos práticos de AEDS 2
date: 2017-07-17
tags: ["aeds2", "exemplos"]
comments: true
---

## Professores

Os trabalhos presentes nessa seção foram desenvolvidos por professores e disponibilizados pelos mesmos.

#### Flavio Figueiredo

Conteúdo completo da disciplina com slides, código dos trabalhos e exemplos.

| Contato | Código Fonte | Site da Disciplina |
| --- | --- | --- |
| [Github](https://github.com/flaviovdf), [Site do DCC](https://www.dcc.ufmg.br/dcc/?q=pt-br/node/2477) | [AEDS2 20171](https://github.com/flaviovdf/AEDS2-2017-1) | [github pages](https://flaviovdf.github.io/AEDS2-2017-1/) |

---

## Alunos

Os trabalhos presentes nessa seção foram desenvolvidos por alunos durante a disciplina. Por isso nem todos apresentam as práticas ideais de codificação ou o  melhor algoritmo para resolução do problema, por isso recomendamos que utilizem os códigos apenas como exemplos.

#### Não temos exemplos de alunos ainda :(
Infelizmente ainda não temos exemplos para trabalhos de AEDS 2 de alunos. Caso você tenha algum trabalho dessa disciplina e esteja disposto a compartilhá-lo, entre em contato. O contato pode ser feito por email ou facebook disponíveis ao clicar nos ícones no rodapé dessa página.

Estamos trabalhando em uma página de contatos para facilitar o contato com o público.

---
title: AEDS 1
subtitle: Alguns exemplos de trabalhos práticos de AEDS 1
date: 2017-07-17
tags: ["aeds1", "exemplos"]
comments: true
---

## Professores

Os trabalhos presentes nessa seção foram desenvolvidos por professores e disponibilizados pelos mesmos.

#### Não temos exemplos de professores ainda :(
Infelizmente ainda não temos exemplos para trabalhos de AEDS 1 de Professores. Caso você tenha algum trabalho dessa disciplina e esteja disposto a compartilhá-lo, entre em contato. O contato pode ser feito por email ou facebook disponíveis ao clicar nos ícones no rodapé dessa página.

---

## Alunos

Os trabalhos presentes nessa seção foram desenvolvidos por alunos durante a disciplina. Por isso nem todos apresentam as práticas ideais de codificação ou o  melhor algoritmo para resolução do problema, por isso recomendamos que utilizem os códigos apenas como exemplos.

### Trabalho Prático 1
| Nome | Contato | Código Fonte | Módulo | Abordagem |
| :-: | :-: | :-: | :-: | :-: |
| Ícaro Harry | [Github](https://github.com/icaroharry/) | [Pong](https://github.com/icaroharry/pong) | Allegro | Pong |

---

Caso você tenha algum trabalho dessa disciplina e esteja disposto a compartilhá-lo, entre em contato. O contato pode ser feito por email ou facebook disponíveis ao clicar nos ícones no rodapé dessa página.

Estamos trabalhando em uma página de contatos para facilitar o contato com o público.

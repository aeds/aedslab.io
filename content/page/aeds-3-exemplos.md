---
title: AEDS 3
subtitle: Alguns exemplos de trabalhos práticos de AEDS 3
date: 2017-07-17
tags: ["aeds3", "exemplos"]
comments: true
---

## Professores

Os trabalhos presentes nessa seção foram desenvolvidos por professores e disponibilizados pelos mesmos.

#### Não temos exemplos de professores ainda :(
Infelizmente ainda não temos exemplos para trabalhos de AEDS 3 de Professores. Caso você tenha algum trabalho dessa disciplina e esteja disposto a compartilhá-lo, entre em contato. O contato pode ser feito por email ou facebook disponíveis ao clicar nos ícones no rodapé dessa página.

---

## Alunos

Os trabalhos presentes nessa seção foram desenvolvidos por alunos durante a disciplina. Por isso nem todos apresentam as práticas ideais de codificação ou o  melhor algoritmo para resolução do problema, por isso recomendamos que utilizem os códigos apenas como exemplos.

### Trabalho Prático 0
| Nome | Contato | Código Fonte | Módulo | Abordagem |
| :-: | :-: | :-: | :-: | :-: |
| Caio Zanatelli | [Github](https://github.com/caiozanatelli) | [Notação Polonesa Reversa](https://github.com/caiozanatelli/RPNSolver) | Inicial | Força Bruta |

### Trabalho Prático 1
| Nome | Contato | Código Fonte | Módulo | Abordagem |
| :-: | :-: | :-: | :-: | :-: |
| Caio Zanatelli | [Github](https://github.com/caiozanatelli) | [Entregando Lanches](https://github.com/caiozanatelli/MaxFlow) | Grafos | Fluxo Máximo |

### Trabalho Prático 2
| Nome | Contato | Código Fonte | Módulo | Abordagem |
| :-: | :-: | :-: | :-: | :-: |
| Caio Zanatelli | [Github](https://github.com/caiozanatelli) | [Índice Invertido](https://github.com/caiozanatelli/ExternalIndexer) | Ordenação Externa | Ordenação Externa |

### Trabalho Prático 3
| Nome | Contato | Código Fonte | Módulo | Abordagem |
| :-: | :-: | :-: | :-: | :-: |
| Caio Zanatelli | [Github](https://github.com/caiozanatelli) | [Legado da Copa](https://github.com/caiozanatelli/LISReduction) | Paradigmas | Força Bruta, Guloso e Programação Dinâmica |

---

Caso você tenha algum trabalho dessa disciplina e esteja disposto a compartilhá-lo, entre em contato. O contato pode ser feito por email ou facebook disponíveis ao clicar nos ícones no rodapé dessa página.

Estamos trabalhando em uma página de contatos para facilitar o contato com o público.

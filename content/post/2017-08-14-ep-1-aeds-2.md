---
title: Exercício de Programação 1 – TipoString
date: 2017-08-14
tags: ["aeds2", "20172", "ep1"]
---

## Exercicio de Tipo Abstrato de Dados (TAD) 

As informações sobre o EP podem ser encontradas na [especificação](/pdf/EP1.pdf).

---

#### Problema Resumido

O exercicio consiste em criar um TAD para representar `Strings` que será utilizado
em dois programas. O primeiro programa é bem simples e busca testar as
funções do TAD enquanto o segundo consiste em criar um programa para para
criptografar um texto segundo **Cifra de Vigenère**.

---

#### Simplificações

O problema apresentado apresenta algumas simplificações:

- O TAD apresenta frases formadas apenas por caracteres de [A .. Z].
- O tamanho máximo da string é de 50 caracteres. (O uso de alocação dinâmica não
é obrigatório)

---

#### Observações

Entenda como retorno `NULL` na especificação uma string vazia.

É **obrigatório** a implementação de **testes de consistência** como verificar
caracteres válidos, índices de acesso e afins.

A assinatura das funções estão na especificação e **não devem** ser alteradas com
exceção de erro como na operação 5 onde há ausencia de uma virgula, o que resultaria
em erro de sintaxe.

---

As informações acima podem ser vistas na [especificação](/pdf/EP1.pdf) de forma
muito mais completa.

Daqui em diante serão apresentados informações e dicas para solução do problema,
por isso recomendamos fortemente que antes de acessá-las você tente resolver o
exercício sozinho e caso não consiga volte aqui.

---

#### Dicas

Mais dicas e afins serão adionadas mais tarde. Coloquei essa dica com pressa
devido a duvida de alguns alunos.

<details>
  <summary>
    Para verificar se um caractere está dentro dos padrões de [A .. Z] podemos
    utilizar um `if`
    (clique aqui para acessar um exemplo)
  </summary>

  Os valores de [A .. Z] na tabela asc são ordinais e consecutivos, ou seja,
  para verifcar se um caractere está em [A .. Z] basta ver se ele tem valor
  maior ou igual a `A` **e** menor ou igual a `Z`. 
  
  ``` c
if(c >= 'A' && c <= 'Z') {
    // o caractere da variavel c esta entre [A .. Z]
}
  ```
</details>

<details>
  <summary>
    A cifra pode ser reduzida a uma equação a ser realizadas com as funções do
    TAD
    (clique aqui para acessar um exemplo)
  </summary>

  Para realizar a cifra vamos usar algumas informações:
  
  - O valor do caractere `A` na tabela ASCII é 65.
  - Quando o valor da soma entre os caracteres é maior que `Z` é necessário que
  o valor volte a `A`, para isso vamos usar o operador `%` (mod) com valor 26,
  uma vez que, são 26 caracteres distintos.
  - Para normalizar o valor dos caracteres vamos realizar a operação `- 'A'` nas
  variáveis da palavra a ser cifrada e `- 'A' + 1` nas variáveis da cifra. Dessa forma
  na palavra a ser cifrada `A` tem valor 0, `B` tem valor 1... e assim por diante,
  enquanto na cifra `A` tem valor 1, `B` tem valor 2 ... e assim por diante.
  - Utilizando isso vamos obter o caractere resultante normalizado, que somado
  a `A` temos o caractere resultante.

  ``` c
Matematica da Cifra:
    Sendo as variáveis:
        CP o caracter da palavra inicial
        CC o caracter da cifra
        CR o carater resultante normalizado
    Temos:
        CR = (CP - 'A' + CC - 'A' + 1) % 26 + 'A'
        CR = (CP + CC - 2 * 'A' + 1) % 26 + 'A'
    Onde substituindo 'A' por seu valor 65 temos:
        CR = (CP + CC - 129) % 26 + 65
  ```
</details>

---

#### Código

<details>
  <summary>
    Código exemplo do EP1.c e saída esperada (clique aqui para acessar um exemplo)
  </summary>

  Caso você não tenho utilizado o return `NULL` modifique a primeira função.

  ``` c
#include <stdio.h>
#include "TipoString.h"

int main(int argc, char *argv[]) {
    TipoString string;
    char c;
    int verificador;

    // tenta criar string e falha, retornando NULL
    string = CriaString("HELLO WORLD");
    if(GetTamanho(string) == 0) {
        printf("A string eh nula\n");
    }

    // cria uma nova String com o texto HELLOWORLD
    string = CriaString("HELLOWORLD");
    printf("A string eh: ");
    ImprimeString(string);

    // tenta pegar o caractere da terceira posicao da string
    c = GetChar(string, 3);
    printf("O caractere na posicao 3 eh: ");
    printf("%c\n", c);

    // tenta pegar um caractere fora do limite da string
    c = GetChar(string, 43);
    printf("O caractere na posicao 43 eh: ");
    printf("%c\n", c);
    c = GetChar(string, -1);
    printf("O caractere na posicao -1 eh: ");
    printf("%c\n", c);

    // tenta modificar o caractere da quinta posicao da string para um caractere
    // invalido
    verificador = SetChar(&string, 5, 'a');
    if(verificador == 0) {
        printf("A string alterada com sucesso\n");
    } else {
        printf("A string nao foi alterada\n");
    }
    printf("String apos tentar alterar o 5 caractere para a eh: ");
    ImprimeString(string);

    // tenta modificar o caractere fora do limtie' da string para um caractere
    verificador = SetChar(&string, 30, 'F');
    if(verificador == 0) {
        printf("A string alterada com sucesso\n");
    } else {
        printf("A string nao foi alterada\n");
    }
    printf("String apos tentar alterar o 30 caractere para F eh: ");
    ImprimeString(string);


    // tenta modidicar o caractere da quarta posicao da string para S
    verificador = SetChar(&string, 4, 'S');
    if(verificador == 0) {
        printf("A string alterada com sucesso\n");
    } else {
        printf("A string nao foi alterada\n");
    }
    printf("String apos tentar alteras o 4 caractere para S eh: ");
    ImprimeString(string);

    // insere 'A's na string ate que nao seja possivel
    verificador = 0;
    while(verificador == 0) {
        verificador = InsereChar(&string, 'A');
        if(verificador == 0) {
            printf("A string alterada com sucesso\n");
        } else {
            printf("A string nao foi alterada\n");
        }
        printf("String apos tentar inserir A eh: ");
        ImprimeString(string);
    }

    return 0;
}
  ```

  A saída do programa é:

  ```
A string eh nula
A string eh: HELLOWORLD
O caractere na posicao 3 eh: L
O caractere na posicao 43 eh: !
O caractere na posicao -1 eh: !
A string nao foi alterada
String apos tentar alterar o 5 caractere para a eh: HELLOWORLD
A string nao foi alterada
String apos tentar alterar o 30 caractere para F eh: HELLOWORLD
A string alterada com sucesso
String apos tentar alteras o 4 caractere para S eh: HELLSWORLD
A string alterada com sucesso
String apos tentar inserir A eh: HELLSWORLDA
A string alterada com sucesso
String apos tentar inserir A eh: HELLSWORLDAA
A string alterada com sucesso
String apos tentar inserir A eh: HELLSWORLDAAA
A string alterada com sucesso
String apos tentar inserir A eh: HELLSWORLDAAAA
A string alterada com sucesso
String apos tentar inserir A eh: HELLSWORLDAAAAA
A string alterada com sucesso
String apos tentar inserir A eh: HELLSWORLDAAAAAA
A string alterada com sucesso
String apos tentar inserir A eh: HELLSWORLDAAAAAAA
A string alterada com sucesso
String apos tentar inserir A eh: HELLSWORLDAAAAAAAA
A string alterada com sucesso
String apos tentar inserir A eh: HELLSWORLDAAAAAAAAA
A string alterada com sucesso
String apos tentar inserir A eh: HELLSWORLDAAAAAAAAAA
A string alterada com sucesso
String apos tentar inserir A eh: HELLSWORLDAAAAAAAAAAA
A string alterada com sucesso
String apos tentar inserir A eh: HELLSWORLDAAAAAAAAAAAA
A string alterada com sucesso
String apos tentar inserir A eh: HELLSWORLDAAAAAAAAAAAAA
A string alterada com sucesso
String apos tentar inserir A eh: HELLSWORLDAAAAAAAAAAAAAA
A string alterada com sucesso
String apos tentar inserir A eh: HELLSWORLDAAAAAAAAAAAAAAA
A string alterada com sucesso
String apos tentar inserir A eh: HELLSWORLDAAAAAAAAAAAAAAAA
A string alterada com sucesso
String apos tentar inserir A eh: HELLSWORLDAAAAAAAAAAAAAAAAA
A string alterada com sucesso
String apos tentar inserir A eh: HELLSWORLDAAAAAAAAAAAAAAAAAA
A string alterada com sucesso
String apos tentar inserir A eh: HELLSWORLDAAAAAAAAAAAAAAAAAAA
A string alterada com sucesso
String apos tentar inserir A eh: HELLSWORLDAAAAAAAAAAAAAAAAAAAA
A string alterada com sucesso
String apos tentar inserir A eh: HELLSWORLDAAAAAAAAAAAAAAAAAAAAA
A string alterada com sucesso
String apos tentar inserir A eh: HELLSWORLDAAAAAAAAAAAAAAAAAAAAAA
A string alterada com sucesso
String apos tentar inserir A eh: HELLSWORLDAAAAAAAAAAAAAAAAAAAAAAA
A string alterada com sucesso
String apos tentar inserir A eh: HELLSWORLDAAAAAAAAAAAAAAAAAAAAAAAA
A string alterada com sucesso
String apos tentar inserir A eh: HELLSWORLDAAAAAAAAAAAAAAAAAAAAAAAAA
A string alterada com sucesso
String apos tentar inserir A eh: HELLSWORLDAAAAAAAAAAAAAAAAAAAAAAAAAA
A string alterada com sucesso
String apos tentar inserir A eh: HELLSWORLDAAAAAAAAAAAAAAAAAAAAAAAAAAA
A string alterada com sucesso
String apos tentar inserir A eh: HELLSWORLDAAAAAAAAAAAAAAAAAAAAAAAAAAAA
A string alterada com sucesso
String apos tentar inserir A eh: HELLSWORLDAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
A string alterada com sucesso
String apos tentar inserir A eh: HELLSWORLDAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
A string alterada com sucesso
String apos tentar inserir A eh: HELLSWORLDAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
A string alterada com sucesso
String apos tentar inserir A eh: HELLSWORLDAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
A string alterada com sucesso
String apos tentar inserir A eh: HELLSWORLDAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
A string alterada com sucesso
String apos tentar inserir A eh: HELLSWORLDAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
A string alterada com sucesso
String apos tentar inserir A eh: HELLSWORLDAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
A string alterada com sucesso
String apos tentar inserir A eh: HELLSWORLDAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
A string alterada com sucesso
String apos tentar inserir A eh: HELLSWORLDAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
A string alterada com sucesso
String apos tentar inserir A eh: HELLSWORLDAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
A string alterada com sucesso
String apos tentar inserir A eh: HELLSWORLDAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
A string alterada com sucesso
String apos tentar inserir A eh: HELLSWORLDAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
A string nao foi alterada
String apos tentar inserir A eh: HELLSWORLDAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
  ```
</details>
